# Gitlab CI Components

Common gitlab ci components

## Components

### [aws-ecr-docker-auth-config](templates/aws-ecr-docker-auth-config/README.md)

Set `DOCKER-AUTH-CONFIG` environment variable.

### [tf](templates/tf/README.md)

A tf (terraform or opentofu) build pattern.

### [deploy](templates/deploy/README.md)

A generic deploy pattern based on a typical helm deploy.

## Reference
- https://docs.gitlab.com/ee/ci/yaml/
- https://docs.gitlab.com/ee/ci/components/
- 




