# tf

For terraform or opentofu build pattern.

## Usage

In `.gitlab-ci.yml`, include a `ci.yml` of an environment
```yaml
include:
  - local: env/tf/ci.yml
```

In the environment directory, include a `ci.yml` with following content:
```yaml
include:
  - local: templates/tf/ci.yml
    inputs:
      environment_name: tf
      directory: env/tf
      build_docker: hashicorp/terraform:1.6.2
```

```text
├── env
│   └── tf
│       ├── README.md
│       ├── ci.yml
│       └── tf_file.tf
└── .gitlab-ci.yml
```

## Customization

### Inputs
Inputs for the components are:
- `environment_name`: used to name job. e.g. plan:tf
- `directory`: directory of terraform environment relative to repository root directory
- `build_docker`: docker image to use to build this job. If it is a private image, need to run aws-ecr-docker-auth-config job prior. Reference https://medium.com/p/3d61397f9a03
- `build_docker_entrypoint`: entry point for build docker. Default: []
- `gitlab_runner_tag`: gitlab runner to run this job. Default: []
- `save_generated_files`: whether to save generated files in .generated folder if any. Default: “false”
- `script_name`: list of script in .generated/scriptsname to consider to run. Default: NULL_VALUE
- `tf_command`: whether to run terraform or tofu . Default: terraform

### Job
Customize job. Values are examples.
```yaml
# global settings
#################

## put this in Settings -> CI/CD -> variables
# PROJECT_ACCESS_TOKEN # for saving generated file to repo if any

variables:
  JOB_TF_INIT_OPTION: -backend-config=... # for tf init

# job settings
##############
validate:<environment_name>:
  variables:
    TF_OPTION: -target local_file.script # for tf plan

script:<environment_name>:
  variables:
    BUILD_ROLE_ARN: arn:aws:iam::123456789012:role/build-iac # role to run the script
```
