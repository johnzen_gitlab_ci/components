# deploy

A generic deploy build pattern based on a typical helm deploy.

## Usage

In `.gitlab-ci.yml`, include a `ci.yml` of an environment
```yaml
include:
  - local: env/deploy/ci.yml
```

In the environment directory, include a `ci.yml` with following content:
```yaml
include:
  - local: templates/deploy/ci.yml
    inputs:
      environment_name: deploy
      directory: env/deploy
      build_docker: alpine:3.19.1
```

The environment directory is assume to have:
- `.env` to store environment variables to be used to replace settings in `values.yml` file
- `values.yml` file to pass to helm chart or similar

A "call-back" bash script `ci/deploy.sh` is assume to exist.

```text
├── .gitlab-ci.yml
├── ci
│   └── deploy.sh
└── env
    └── deploy
        ├── README.md
        ├── ci.yml
        ├── .env
        └── values.yml
```
## Customization

### Inputs
Inputs for the components are:
- `environment_name`: used to name job. e.g. plan:tf
- `directory`: directory of terraform environment relative to repository root directory
- `build_docker`: docker image to use to build this job. If it is a private image, need to run aws-ecr-docker-auth-config job prior. Reference https://medium.com/p/3d61397f9a03
- `build_docker_entrypoint`: entry point for build docker. Default: []
- `gitlab_runner_tag`: gitlab runner to run this job. Default: []
- `build_role_arn`: aws iam role to run job in this pipeline. Default: NULL_VALUE

### Job

Override a job section e.g. `script` section of `secret_get` job as follow:
```yaml
secret_get:deploy:
  script:
    # Customize this to get secrets from secret manager
    - TOKEN=001122334455
    - echo "TOKEN=${TOKEN}" > secrets.env
```
