# aws-ecr-docker-auth-config

Set `DOCKER-AUTH-CONFIG` for subsequent job's build image pull from private AWS ECR repository.

This is developed for self-managed gitlab server and runner in eks cluster.

## Usage

Include in `.gitlab-ci.yml` e.g.
```yaml
include:
  - project: johnzen_gitlab_ci/components
    ref: main
    file: templates/aws-ecr-docker-auth-config/ci.yml
    inputs:
      docker_registry: 123456789012.dkr.ecr.ap-southeast-2.amazonaws.com
      build_iam_role: arn:aws:iam::123456789012:role/build-ecr
      build_docker: public.ecr.aws/aws-cli/aws-cli:2.15.42
      gitlab_runner_tag: [ my-runner-01 ]
```

## Reference
- https://blog.revolve.team/2024/02/01/private-amazon-ecr-registry-with-gitlab/
- https://mherman.org/blog/gitlab-ci-private-docker-registry/
